let processedAccount;

$("#change-password-form").submit(validateChangePassword);

function validateChangePassword(){
    let errCount = 0;
    let oldPas = $("#changepassword-old").get();
    let newPas = $("#changepassword-new").get();
    let newPas2 = $("#changepassword-new2").get();
    let oldPasTxt = $(oldPas).val().toString();
    let newPassTxt = $(newPas).val().toString();
    let newPassTxt2 = $(newPas2).val().toString();

    //The same as old
    if(oldPasTxt == newPassTxt){
        errCount++;
        invalidInput($(oldPas));
        invalidInputTxt($(newPas), "Nowe hasło jest takie samo, jak stare");
    }
    else{
        deleteInputFeedback($(oldPas));
        deleteInputFeedback($(newPas));
    }

    //Password haven't enought length
    if(newPassTxt.length < 6 || newPassTxt.length > 50){
        errCount++;
        invalidInputTxt($(newPas), "Hasło jest zbyt krótkie");
    }
    else{
        if(errCount == 0) {
            validInput($(newPas));
            deleteInputFeedbackTxt($(newPas));
        }
    }

    //Password heaven't required chars
    if(isInPasswordBigLetters(newPassTxt, 1) && isInPasswordNumbers(newPassTxt, 1)){
        if(errCount == 0){
            validInput($(newPas));
            validInput($(newPas2));
            deleteInputFeedbackTxt($(newPas));
        }
    }
    else{
        errCount++;
        invalidInputTxt($(newPas), "Hasło nie ma wszystkich wymaganych znaków");
        invalidInput($(newPas));
    }

    //The same password
    if(newPassTxt == newPassTxt2){
        if(errCount == 0) {
            validInputTxt($(newPas));
            validInputTxt($(newPas2));
            deleteInputFeedbackTxt($(newPas));
            deleteInputFeedbackTxt($(newPas2));
        }
    }
    else{
        errCount++;
        invalidInputTxt($(newPas), "Hasła nie są takie same");
        invalidInputTxt($(newPas2), "Hasła nie są takie same");
    }

    if(errCount == 0)
        return true;
    else
        return false;
}

function updateAccName(elem){
    let parent = $(elem).parent().get();

    parent = $(parent).find("input").get();
    let id = $(elem).data("idacc");

    if($(parent).val().length < 3){
        invalidInputTxt(parent, "Nazwa jest zbyt krótka");
    }
    else{
        window.location.href = "?page=updateMoneyAccount&name=" + $(parent).val() + "&id_acc=" + id;
    }
    return false;
}

function executeDeleteAccount(){
    window.location.href = "?page=deleteMoneyAccount&id=" + processedAccount;
}

function deleteAccount(elem){
    processedAccount = $(elem).data("idacc");
}

