<!doctype html>
<html lang="pl" class="max-window-height">
<head>
    <?php /** @var array $variables */
    require_once("Views/blocks/head.html"); ?>
</head>
<body class="white-background max-window-height">
<?php
require_once "Views\blocks\leftmenu.html"; ?>

<div class="content styled-scrollbar">
    <div class="container">
        <div class="row">
            <div class="dashboard-background col-12">
                <div class="row">
                    <h2 class="text-center col-12">Ustawienia</h2>
                </div>
                <div class="row mt-5">
                    <h3 class="col-12">Lista kont</h3>
                    <div class="col-lg-4 col-md-6">
                        <div class="list-group" id="list-tab" role="tablist">
                            <?php foreach ($variables['accountsList'] as $row): ?>
                                <a class="list-group-item list-group-item-action"
                                   id="list-acc-id-<?= $row['id_m_acc'] ?>" data-toggle="list"
                                   href="#list-acc-content-id-<?= $row['id_m_acc'] ?>"
                                   role="tab"><?= $row['name'] ?></a>
                            <?php endforeach; ?>
                            <div class="list-group-item">
                                <form method="post" action="?page=addMoneyAccount" class="d-flex">
                                        <input type="text" class="form-control mt-1 mr-5" minlength="3" placeholder="Nazwa konta" name="newAcc">
                                        <input type="submit" class="btn btn-success ml-auto" value="Dodaj">
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6">
                        <div class="tab-content" id="nav-tabContent">
                            <?php foreach ($variables['accountsList'] as $row): ?>
                                <div class="tab-pane fade" id="list-acc-content-id-<?= $row['id_m_acc'] ?>"
                                     role="tabpanel">
                                    <input type="text" class="form-control col-6 -col-sm-4" name="list-acc-content-name"
                                           value="<?= $row['name'] ?>" title="Nazwa konta">
                                    <button class="btn btn-success mt-3" data-idacc="<?= $row['id_m_acc'] ?>" onclick="updateAccName(this)">
                                        <i class="material-icons align-middle">save</i> <span class="align-middle">Zapisz</span>
                                    </button>
                                    <button class="btn btn-danger mt-3 ml-5" data-idacc="<?= $row['id_m_acc'] ?>" onclick="deleteAccount(this)"
                                            data-toggle="modal" data-target="#modalConfirmDeleteMoneyAccount">
                                        <i class="material-icons align-middle">delete</i> <span class="align-middle">Usuń</span>
                                    </button>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <h3 class="col-12">Edytor kategorii</h3>
                    <div class="col-12">
                        <a class="btn btn-warning " href="?page=settingsCatEditor"><i
                                    class="material-icons align-middle">edit</i><span class="align-middle">Otwórz edytor kategorii</span></a>
                    </div>
                </div>
                <div class="row mt-5">
                    <h3 class="col-12">Zmień hasło</h3>
                    <?php if ($variables['changePasswordInfo'] != "") : ?>
                        <div class="col-12 mt-3">
                            <h5 class="text-primary"><?= $variables['changePasswordInfo'] ?></h5>
                        </div>
                    <?php endif; ?>
                    <form class="col-12" method="post" action="?page=changePassword" id="change-password-form">
                        <div class="form-group">
                            <label for="changepassword-old">Stare hasło</label>
                            <input type="password" id="changepassword-old" class="form-control col-12 col-sm-4"
                                   name="old_password"
                                   placeholder="Stare hasło" required>
                        </div>

                        <div class="form-group mt-5">
                            <p>Nowe hasło musi zawierać:</p>
                            <ul>
                                <li>minumum 6 znaków</li>
                                <li>minumum 1 dużą literę</li>
                                <li>minumum 1 cyfrę</li>
                            </ul>
                        </div>

                        <div class="form-group">
                            <label for="changepassword-new">Nowe hasło</label>
                            <input type="password" id="changepassword-new" class="form-control col-12 col-sm-4"
                                   name="new_password"
                                   placeholder="Nowe hasło" required>
                        </div>
                        <div class="form-group">
                            <label for="changepassword-new2">Powtórz nowe hasło</label>
                            <input type="password" id="changepassword-new2" class="form-control col-12 col-sm-4"
                                   name="new_password2"
                                   placeholder="Powtórz nowe hasło" required>
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Zatwierdź"
                                   class="form-control btn-warning col-md-2 col-sm-3 col-12">
                        </div>
                    </form>
                </div>
                <div class="row mt-5">
                    <h3 class="col-12">Usuń konto</h3>
                    <div class="col-12">
                        <button class="btn btn-danger" data-toggle="modal" data-target="#modalConfirmDeleteUserAccount"><i class="material-icons align-middle">delete_forever</i><span
                                    class="align-middle">Usuń konto</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Confirm delete money account -->
<div class="modal fade" id="modalConfirmDeleteMoneyAccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Czy na pewno?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6>Czy na pewno chcesz usunąć to konto?</h6>
                <p>Usunięcie konta jest nieodwracalne, a wszystkie wpisy przypisane do konta zostaną przypisane do wirtualnego konta "Usunięte konto".</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="executeDeleteAccount()"><i class="material-icons align-middle">delete</i> Usuń</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
            </div>
        </div>
    </div>
</div>

<!-- Confirm delete user account -->
<div class="modal fade" id="modalConfirmDeleteUserAccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Czy na pewno?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6>Czy na pewno chcesz usunąć swoje konto?</h6>
                <p>Usunięcie konta jest nieodwracalne, wszystkie Twoje dane zostaną usunięte!</p>
                <form method="post" action="?page=deleteAccount" id="form-delete-user">
                    <label for="passwordDeleteAccount">Wpisz hasło, aby potwierdzić usuwanie konta:</label>
                    <div class="d-flex">
                        <input type="password" name="password" class="form-control col-7" id="passwordDeleteAccount">
                        <button type="submit" class="btn btn-danger ml-auto" form="form-delete-user">
                            <i class="material-icons align-middle">delete</i> Usuń
                        </button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
            </div>
        </div>
    </div>
</div>

<!-- Notify -->
<div class="notify-block">

    <?php require_once "Views\blocks\cookieInformationNotify.html";?>
</div>

<!-- Modal window -->
<?php /** @noinspection PhpIncludeInspection */
require_once "Views\blocks\modals.php"; ?>
<?php /** @noinspection PhpIncludeInspection */
require_once "Views\blocks\bootstrapFooter.html"; ?>

<?php if($variables['infModal'] == 1) :  ?>
    <!-- information modal -->

    <div class="modal fade show" id="modalInformation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><?= $variables['infModalTxt']?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

<script>
        $("#modalInformation").modal('show');
</script>

<?php endif; ?>

<script src="Views/js/settings.js"></script>
</html>