<!doctype html>
<html lang="pl" class="max-window-height">
<head>
    <?php /** @var array $variables */
    require_once("Views/blocks/head.html"); ?>
</head>
<body class="white-background max-window-height">
<?php
require_once "Views\blocks\leftmenu.html"; ?>
<div class="content styled-scrollbar">
    <div class="container">
        <div class="row">
            <div class="dashboard-background col-12">
                <div class="row">
                    <h2 class="text-center col-12">Edytor kategorii</h2>
                </div>
                <div class="row mt-5">
                    <h3 class="col-12">Lista kont</h3>
                    <div class="col-md-6 col-lg-4">
                        <div class="list-group" id="list-tab" role="tablist">
                            <?php foreach ($variables["categoryTable"] as $row) : ?>
                                <a class="list-group-item list-group-item-action"
                                   id="list-acc-id-<?= $row['id_category'] ?>"
                                   data-toggle="list" href="#list-acc-content-id-<?= $row['id_category'] ?>"
                                   onclick="updateProcessedCategory(<?= $row['id_category'] ?>)"
                                   role="tab"><?= $row['name'] ?></a>
                            <?php endforeach; ?>
                            <div class="list-group-item list-group-item-action">
                                <form class="d-flex" method="post" action="?page=settingsNewCategory">
                                    <input type="text" name="newSubcat" placeholder="Nowa kategoria"
                                           class="mt-1 form-control mr-5" pattern=".{3,20}"
                                           title="Nazwa kategorii musi mieć mninimum 3 znaki, a maksimum 20" required>
                                    <input type="submit" class="btn btn-success ml-auto" value="Dodaj">
                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6 col-lg-8">
                        <div class="tab-content" id="nav-tabContent">
                            <?php foreach ($variables["categoryTable"] as $row) : ?>
                                <div class="tab-pane fade" id="list-acc-content-id-<?= $row['id_category'] ?>"
                                     role="tabpanel">
                                    <div class="d-flex">
                                        <div class="dropdown p-2">
                                            <button class="btn btn-secondary dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false">
                                                <div class="dropdown-color-div"
                                                     style="background: <?= $row['color'] ?>"></div>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-color styled-scrollbar styled-scrollbar-white-bg"
                                                 aria-labelledby="dropdownMenuButton">
                                                <?php foreach ($variables['colorList'] as $color) : ?>
                                                    <div class="dropdown-item" onclick="updateColor(this)">
                                                        <div class="dropdown-color-div"
                                                             style="background: <?= $color['color'] ?>"></div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                        <div class="dropdown p-2">
                                            <button class="btn btn-primary dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false">
                                                <?php if ($row['is_income'] == 0) echo "Wydatek"; else echo "Przychód"; ?>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#" onclick="categoryToExpense(this)">Wydatek</a>
                                                <a class="dropdown-item" href="#" onclick="categoryToIncome(this)">Przychód</a>
                                            </div>
                                        </div>
                                        <div class="ml-auto p-2">
                                            <button class="btn btn-danger" data-toggle="modal"
                                                    data-target="#confirm-delete-category"
                                                    data-idcategory="<?= $row['id_category'] ?>"
                                                    onclick="deleteCategoryClick(this)">
                                                <i class="material-icons align-middle">delete_forever</i>
                                                Usuń
                                            </button>
                                        </div>
                                    </div>

                                    <div class="list-group">
                                        <div class="list-group-item list-group-item-action active disabled">
                                            <div class="d-flex">
                                                <h4 class="item-right-part mt-2 pb-2"><?= $row["subList"][0]["name"] ?></h4>
                                                <button class="btn btn-warning ml-auto" onclick="editCategoryName(this)"
                                                        data-target="#edit-category-name-window" data-toggle="modal"
                                                        data-idsubcategory="<?= $row["subList"][0]['id_sub_cat'] ?>"><i
                                                            class="material-icons align-middle">edit</i>
                                                    Edytuj
                                                </button>
                                            </div>
                                        </div>
                                        <?php for ($i = 1; $i < count($row["subList"]); $i++) : $sub = $row["subList"][$i]; ?>
                                            <div class="list-group-item list-group-item-action">
                                                <div class="d-flex">
                                                    <div class="item-thunbail align-self-center"
                                                         style="background:<?= $row["color"] ?>">
                                                        <?php $t = mb_substr($row["name"], 0, 1, 'UTF-8'); echo $t;?>
                                                    </div>
                                                    <h5 class="item-right-part mt-1"><?= $sub["name"] ?></h5>
                                                    <button class="btn btn-warning ml-auto" data-toggle="modal"
                                                            data-target="#edit-subcategory-window"
                                                            data-idsubcategory="<?= $sub['id_sub_cat'] ?>"
                                                            onclick="editSubCategoryNameClick(this)"><i
                                                                class="material-icons align-middle">edit</i>
                                                        Edytuj
                                                    </button>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                        <div class="list-group-item list-group-item-action">
                                            <div class="d-flex">
                                                <div class="item-thunbail align-self-center"
                                                     style="background:<?= $row["color"] ?>">
                                                    <?php $t = mb_substr($row["name"], 0, 1, 'UTF-8'); echo $t;?>
                                                </div>
                                                <input type="text" name="newSubcat" placeholder="Nowa podkategoria"
                                                       class="mt-1 form-control ml-3 mr-5" pattern=".{3,20}">
                                                <button class="btn btn-success ml-auto"
                                                        data-idcategory="<?= $row['id_category'] ?>"
                                                        onclick="addSubcategory(this)">Dodaj
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal window delete category -->
<div class="modal fade" id="confirm-delete-category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4>Czy na pewno chcesz usunąć tę kategorię?</h4>
                <p>Do jakiej kategorii maja zostać przepisane wydatki?</p>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="delete-category-list">Kategoria</label>
                            <select class="form-control" id="delete-category-list">
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="delete-subcategory-list">Podkategoria</label>
                            <select class="form-control" id="delete-subcategory-list">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" onclick="deleteCategoryExecute()"><i
                            class="material-icons align-middle">delete_forever</i>
                    Usuń
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Anuluj</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit subcategory name -->
<div class="modal fade" id="edit-subcategory-window" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-body-subcategory-edit">
                <div class="form-group">
                    <label for="newSubcategoryname">Nazwa podkategorii:</label>
                    <input class="form-control" type="text" value="" name="newSubcategoryname" id="newSubcategoryname" pattern=".{3,20"
                           title="Długość nazwy, to od 3 do 20 znaków">
                </div>
                <div class="col-12">
                    <p>Usuwając podkategorię, wszystkie przypisane do niej wpisy zostana przepisane do podkategorii
                        ogólnie w tej kategorii.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger mr-auto" onclick="deleteSubcategoryExecute()" data-dismiss="modal">
                    <i class="material-icons align-middle">delete_forever</i>Usuń
                </button>
                <button class="btn btn-success" onclick="editSubCategoryExecute()" data-dismiss="modal"><i
                            class="material-icons align-middle">done</i>Zmień
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Anuluj</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit category name -->
<div class="modal fade" id="edit-category-name-window" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-body-category-edit">
                <div class="form-group">
                    <label for="newSubcategorynameInput">Nazwa podkategorii:</label>
                    <input class="form-control" type="text" value="" name="newSubcategoryname" pattern=".{3,20"
                           title="Długość nazwy, to od 3 do 20 znaków" id="newSubcategorynameInput">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" onclick="editCategoryExecute()" data-dismiss="modal"><i
                            class="material-icons align-middle">done</i>Zmień
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Anuluj</button>
            </div>
        </div>
    </div>
</div>

<!-- Notify -->
<div class="notify-block">

    <?php require_once "Views\blocks\cookieInformationNotify.html";?>
</div>
<?php /** @noinspection PhpIncludeInspection */
require_once "Views\blocks\modals.php"; ?>
<?php /** @noinspection PhpIncludeInspection */
require_once "Views\blocks\bootstrapFooter.html"; ?>

<script src="Views/js/categoryEditor.js">
</script>
</html>