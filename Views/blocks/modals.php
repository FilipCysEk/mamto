<!-- Modal window -->
<?php /** @var array $variables */?>

<div aria-labelledby="modal-new-item" class="modal fade" id="modal-new-item" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg modal-lg-my" role="document">
        <form class="modal-content" method="post" action="?page=addNewExpense">
            <input type="hidden" name="page" value="<?= $_GET["page"] ?>">
            <div class="modal-header">
                <h5 class="modal-title">Nowy wpis</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="modal-body-cont"></div>
            <div class="modal-body pt-0 pb-0" id="modal-body-expense-income">
                <div class="row" style="margin-left:-16px;">
                    <div class="col-md-7 m-0 fushia-background py-4">
                        <div class="row">
                            <div class="form-group col-5 offset-1">
                                <label for="modal-new-item-type-item">Typ wpisu</label>
                                <select class="form-control" id="modal-new-item-type-item" name="type-item" required  onchange="updateModalContent(this); updateCategory(this)">
                                    <option value="income">Przychód</option>
                                    <option value="expense" selected>Wydatek</option>
                                    <option value="transaction">Przelew</option>
                                </select>
                            </div>
                            <div class="form-group col-5 offset-0">
                                <label for="modal-new-item-acc-item">Konto</label>
                                <select class="form-control" id="modal-new-item-acc-item" name="acc-item" required>
                                    <?php foreach ($variables['newExpense']["account_list"] as $acc) : ?>
                                        <option value="<?= $acc["id_m_acc"] ?>"><?= $acc["name"] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-4 offset-4">
                                <label for="modal-new-item-money-item">Kwota</label>
                                <div class="input-group money-div">
                                    <input type="number" class="form-control" name="money-item"
                                           id="modal-new-item-money-item" min="0.00" step="0.01" required>
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">zł</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-5 offset-1">
                                <label for="modal-new-item-cat-item">Kategoria</label>
                                <select class="form-control" id="modal-new-item-cat-item" name="cat-item" required onchange="updateSubcategory(this)">

                                </select>
                            </div>
                            <div class="form-group col-5 offset-0">
                                <label for="modal-new-item-subcat-item">Podkategoria</label>
                                <select class="form-control" id="modal-new-item-subcat-item" name="subcat-item"
                                        required>
                                    <option value="0">Ogólnie - jedzenie</option>
                                    <option value="1">Artykuły spżywcze</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 py-4">
                        <div class="row col-10 offset-1 form-group">
                            <label for="modal-new-item-date-item">Data</label>
                            <input type="datetime-local" class="form-control" id="modal-new-item-date-item" name="date-item">
                        </div>
                        <div class="row form-group col-12 mt-5 ml-1">
                            <label for="modal-new-item-description-item">Opis</label>
                            <textarea name="describe-item" class="textarea-noresizible form-control"
                                      id="modal-new-item-description-item" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body pt-0 pb-0" id="modal-body-transaction">
                <div class="row" style="margin-left:-16px;">
                    <div class="col-md-7 m-0 fushia-background py-4">
                        <div class="row">
                            <div class="form-group col-6 offset-3">
                                <label for="modal-new-item-type-item2">Typ wpisu</label>
                                <select class="form-control" id="modal-new-item-type-item2" name="type-item" required onchange="updateModalContent(this)">
                                    <option value="income">Przychód</option>
                                    <option value="expense">Wydatek</option>
                                    <option value="transaction">Przelew</option>
                                </select>
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group col-4 offset-4">
                                <label for="modal-new-item-money-item">Kwota</label>
                                <div class="input-group money-div">
                                    <input type="number" class="form-control" name="money-item"
                                           id="modal-new-item-money-item" min="0.00" step="0.01" required>
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">zł</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-5 offset-0">
                                <label for="modal-new-item-acc-source">Konto źródłowe</label>
                                <select class="form-control" id="modal-new-item-acc-source" name="acc-source" required>
                                    <?php foreach ($variables['newExpense']["account_list"] as $acc) : ?>
                                        <option value="<?= $acc["id_m_acc"] ?>"><?= $acc["name"] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-5 offset-1">
                                <label for="modal-new-item-acc-dest">Konto docelowe</label>
                                <select class="form-control" id="modal-new-item-acc-dest" name="acc-dest" required>
                                    <?php foreach ($variables['newExpense']["account_list"] as $acc) : ?>
                                        <option value="<?= $acc["id_m_acc"] ?>"><?= $acc["name"] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 py-4">
                        <div class="row col-10 offset-1 form-group">
                            <label for="modal-new-item-date-item2">Data</label>
                            <input type="datetime-local" class="form-control" id="modal-new-item-date-item" name="date-item">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!--<input type="submit" class="btn btn-primary" value="Dodaj">-->
                <input type="button" class="btn btn-primary" value="Dodaj" id="new-expense-window-btn-add">
                <input type="button" class="btn btn-primary" value="Dodaj następny" id="new-expense-window-btn-addNext">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
            </div>
        </form>
    </div>
</div>


<!--TODO: walidacja-->
<script>
    setDefDate();
    let typeInput = 1;
    let trans = document.getElementById("modal-body-transaction").cloneNode(true);
    let exinc = document.getElementById("modal-body-expense-income").cloneNode(true);

    document.getElementById("modal-body-transaction").remove();
    document.getElementById("modal-body-expense-income").remove();

    let body = document.getElementById("modal-body-cont");

    changeModalContentToExpInc();

    function updateModalContent(elem){
        if(elem.options[elem.selectedIndex].value == "transaction") {
            changeModalContentToTransaction();
            document.getElementById("modal-new-item-type-item2").selectedIndex = elem.selectedIndex;
        }
        else{
            changeModalContentToExpInc();
            document.getElementById("modal-new-item-type-item").selectedIndex = elem.selectedIndex;

        }
    }

    function changeModalContentToTransaction(){
        if(typeInput === 0) {
            //document.getElementById("modal-body-transaction").style.display = "block";
            //document.getElementById("modal-body-expense-income").style.display = "none";
            removeAllChild(body);
            body.appendChild(trans);

            typeInput = 1;
            setDefDate();
        }
    }

    function changeModalContentToExpInc(){
        if(typeInput == 1){
            //document.getElementById("modal-body-transaction").style.display = "none";
            //document.getElementById("modal-body-expense-income").style.display = "block";
            removeAllChild(body);
            body.appendChild(exinc);

            typeInput = 0;
        }
    }

    function removeAllChild(elem){
        while (elem.firstChild){
            elem.removeChild(elem.firstChild);
        }
    }

    function setDefDate(){
        let date = new Date();
        date = date.getFullYear() + "-" + (date.getMonth() < 10 ? "0" + (date.getMonth()+1) : date.getMonth()+1) +
            "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) +
            "T" + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) +
            ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes());
        //console.log(date);
        document.getElementById("modal-new-item-date-item").value = date;
        //document.getElementById("modal-new-item-date-item2").value = date;

    }

</script>

