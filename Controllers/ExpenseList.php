<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 13.01.2019
 * Time: 11:30
 */

namespace controllers;


use Model\Expenses;

class ExpenseList extends AppController
{
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->isLogedinAndMove();

        $this->model = new Expenses();
    }

    public function index()
    {
        $id_user = $this->getUserId();

        $expenseList = $this->model->getExpenseList($id_user);
        $money = $this->model->getMoneyOnAllAccounts($id_user);
        $accounts = $this->model->getAccountsList($id_user);
        $categoryList = array_merge_recursive($this->model->getCategoryListIncome($id_user), $this->model->getCategoryListExpense($id_user));


        $this->render("index", ['expenses' => $expenseList, 'sumMoney' => $money, 'accounts' => $accounts, 'categoryList' => $categoryList]);
    }

    public function ajaxFilterExpenses()
    {
        if ($this->isPost()) {
            //print_r($_POST);
            //if ($_POST['dateFrom'] == null && $_POST['dateTo'] == null && $_POST['account'] == null && $_POST['category'] == null)
             //   $this->jsonReturnError("Error receiving data");

            $res = $this->model->getFilteredExpenses($this->getUserId(), $_POST['dateFrom'], $_POST['dateTo'], $_POST['account'], $_POST['category']);
            $this->jsonReturnSuccess($res);
        } else
            $this->jsonReturnError("No data recived");
    }

    public function ajaxGetLast10Item()
    {
        $res = $this->model->getExpenseList($this->getUserId(), 10);
        $this->jsonReturnSuccess($res);
    }

}