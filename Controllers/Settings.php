<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 29.12.2018
 * Time: 18:45
 */

namespace controllers;


use Model\Expenses;

class Settings extends AppController
{
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new \Model\Settings();
        $this->isLogedInAndMove();
    }

    public function index()
    {
        $passInfo = "";
        if(isset($_GET['passChange'])){
            $inf = $_GET['passChange'];
            if($inf == "0")
                $passInfo = "Hasło nie spełnia wymagań";
            elseif ($inf == "2")
                $passInfo = "Podane złe stare hasło";
            elseif ($inf == "1"){
                $passInfo = "Hasło zmieniono poprawnie";
            }

        }

        $infModal = 0;
        $infModalText = "";
        if(isset($_GET['delAcc'])){
            $infModal = 1;
            if($_GET['delAcc'] == true)
                $infModalText = "Błąd podczas usuwania konta! Podano złe hasło!";
            else
                $infModalText = "Błąd podczas usuwania konta!";
        }

        if(isset($_GET['register'])){
            $infModal = 1;
            if($_GET['register'] == "success")
                $infModalText = "Witaj! Znajdujesz sie teraz na podstronie ustawień, możesz tutaj zarządzać swoim kontem i kontami pienięznymi." .
                    "<br>Automatycznie dla Ceibie utworzyliśmy konto Gotówka, ale możesz je usunąć, jak Ci sie nie podoba." .
                    "<br>Stworzyliśmy też Listę kategorii i podkategorii, które możesz wyedytować w edytorze kategorii." .
                    "<br><br>No to zaczynaj!☺";
        }


        $modelExp = new Expenses();
        $accList = $modelExp->getAccountsList($this->getUserId());
        $this->render("index", ['changePasswordInfo' => $passInfo, "accountsList" => $accList, 'infModal' => $infModal, 'infModalTxt' => $infModalText]);
    }

    public function categoryEditor()
    {
        $catlist = array();

        $catlist = $this->model->getAllCategoryList($this->getUserId());

        $colorList = $this->model->getColorsList();

        $modelExp = new Expenses();

        for ($i = 0; $i < count($catlist); $i++) {
            $catlist[$i]["subList"] = $modelExp->getSubCategoryList($catlist[$i]["id_category"]);
        }
        /*
                echo "<pre>";
                print_r($catlist);
                echo "</pre>";
        */
        $this->render("CategoryEditor", ['categoryTable' => $catlist, 'colorList' => $colorList]);
    }

    public function newCategory()
    {
        if ($this->isPost()) {
            if (isset($_POST['newSubcat']) && !EMPTY($_POST['newSubcat'])) {
                $this->model->insertNewCategory($_POST['newSubcat'], $this->getUserId());
                header("Location:?page=settingsCatEditor");
            } else
                $this->throwErrorPage(4);
        } else {
            $this->throwErrorPage(4);
        }
        exit();
    }

    public function deleteCategory()
    {
        if ($this->isGet()) {
            if (isset($_GET['id_cat']) && !EMPTY($_GET['id_cat']) && isset($_GET['id_subcat']) && !EMPTY($_GET['id_subcat'])) {
                $this->model->deleteCategory($_GET['id_cat'], $_GET['id_subcat']);
                header("Location:?page=settingsCatEditor");
                print_r($_GET);
                exit();
            } else
                $this->throwErrorPage(4);
        } else {
            $this->throwErrorPage(4, "No found Get");
        }
    }

    public function addSubcategory()
    {
        if (isset($_POST['id_cat']) && !EMPTY($_POST['id_cat']) && isset($_POST['name']) && !EMPTY($_POST['name'])) {
            $this->model->addSubcategory($_POST['name'], $_POST['id_cat']);

            $this->jsonReturnSuccess();
        } else {
            $this->jsonReturnError();
        }
    }

    public function editSubcategoryName()
    {
        if (isset($_POST['id_sub']) && !EMPTY($_POST['id_sub']) && isset($_POST['name']) && !EMPTY($_POST['name'])) {
            $this->model->editSubcategoryName($_POST['id_sub'], $_POST['name']);

            $this->jsonReturnSuccess();
        } else {
            $this->jsonReturnError();
        }
    }

    public function editCategoryName()
    {
        if (isset($_GET['id_sub']) && !EMPTY($_GET['id_sub']) && isset($_GET['name']) && !EMPTY($_GET['name'])) {
            $this->model->editCategoryName($_GET['id_sub'], $_GET['name']);

            header("Location:?page=settingsCatEditor");
        }
    }

    public function deleteSubcategory()
    {
        if (isset($_POST['id_sub']) && !EMPTY($_POST['id_sub'])) {
            $this->model->deleteSubcategory($_POST['id_sub']);

            $this->jsonReturnSuccess();
        } else {
            $this->jsonReturnError();
        }
    }

    public function categoryToIncome()
    {
        if (isset($_POST['id_cat']) && !EMPTY($_POST['id_cat'])) {
            $this->model->changeCategoryType($_POST['id_cat'], 1);

            $this->jsonReturnSuccess();
        } else {
            $this->jsonReturnError();
        }
    }

    public function categoryToExpense()
    {
        if (isset($_POST['id_cat']) && !EMPTY($_POST['id_cat'])) {
            $this->model->changeCategoryType($_POST['id_cat'], 0);

            $this->jsonReturnSuccess();
        } else {
            $this->jsonReturnError();
        }
    }

    public function categoryUpdateColor()
    {
        if (isset($_POST['id_cat']) && !EMPTY($_POST['id_cat']) && isset($_POST['color']) && !EMPTY($_POST['color'])) {
            $this->model->categoryUpdateColor($_POST['id_cat'], $_POST['color']);

            $this->jsonReturnSuccess();
        } else {
            $this->jsonReturnError();
        }
    }

    public function getCategoryListWOutDeleted()
    {
        if (isset($_POST['id_cat']) && !EMPTY($_POST['id_cat'])) {
            $res = $this->model->getCategoryListWOutDeleted($this->getUserId(), $_POST['id_cat']);

            $this->jsonReturnSuccess($res);
        } else {
            $this->jsonReturnError();
        }
    }

    public function changePassword()
    {
        if (isset($_POST['old_password']) && !EMPTY($_POST['old_password']) &&
            isset($_POST['new_password']) && !EMPTY($_POST['new_password']) &&
            isset($_POST['new_password2']) && !EMPTY($_POST['new_password2'])) {

            if (!$this->changePasswordValidate($_POST['old_password'], $_POST['new_password'], $_POST['new_password2'])) {
                header("Location:?page=settings&passChange=0");
                exit();
            }

            $res = $this->model->changePassword($this->getUserId(), $_POST['old_password'], $_POST['new_password']);
            if ($res === true) {
                header("Location:?page=settings&passChange=1");
            } else {
                header("Location:?page=settings&passChange=2");
            }

        } else {
            $this->throwErrorPage(4, "No recived data");
        }
    }

    private function changePasswordValidate($old_pass, $new_pass, $new_pass2)
    {
        if ($old_pass == $new_pass)
            return false;

        if ($new_pass != $new_pass2)
            return false;

        if (strlen($new_pass) < 6 || strlen($new_pass) > 50)
            return false;

        if (!$this->checkBigLettersInString($new_pass, 1))
            return false;

        if (!$this->checkBigNoumbersInString($new_pass, 1))
            return false;

        return true;
    }

    public function deleteMoneyAccount(){
        if(isset($_GET['id']) && !EMPTY($_GET['id'])){
            $this->model->deleteMoneyAccount($_GET['id'], $this->getUserId());
            header("Location:?page=settings");
        }
        else
            $this->throwErrorPage(4, "No recived data");
    }

    public function ajaxAddMoneyAccount(){
        if(isset($_POST['newAcc']) && !EMPTY($_POST['newAcc'])){
            $this->model->createMoneyAccount($_POST['newAcc'], $this->getUserId());
            header("Location:?page=settings");
        }
        else
            $this->throwErrorPage(4, "No recived data");
    }

    public function ajaxUpdateNameMoneyAccount(){
        if(isset($_GET['name']) && !EMPTY($_GET['name']) && isset($_GET['id_acc']) && !EMPTY($_GET['id_acc'])){
            $this->model->updateMoneyAccount($_GET['id_acc'], $_GET['name']);
            //exit();
            header("Location:?page=settings");
        }
        else
            $this->throwErrorPage(4, "No recived data");
    }

    public function deleteUserAccount(){
        if(isset($_POST['password']) && !EMPTY($_POST['password'])){
            if($this->model->deleteUser($_POST['password'], $this->getUserId())){
                $defContr = new DefaultController();
                $defContr->logout(false);
                header("Location:?page=index&delUser=true");
            }
            else {
                header("Location:?page=settings&delAcc=false");
            }
        }
        else
            $this->throwErrorPage(4, "No recived data");
    }

}