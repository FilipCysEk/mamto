<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 24.11.2018
 * Time: 13:43
 */

namespace Model;

use controllers\AppController;
use PDO;
use PDOException;


class Login extends Database
{
    public function __construct()
    {
        parent::__construct();
    }

    public function login($email, $password){
        $pdo = $this->connect();

        $sql = "SELECT id_user FROM " . NAME_PREFIX . "users WHERE LOWER(email) = LOWER(:email) AND password = :pass AND active = 1";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":email", $email, PDO::PARAM_STR, 50);
        $stm->bindParam(":pass", md5($password), PDO::PARAM_STR, 75);
        $stm->execute();

        $result = $stm->fetchAll();

        return $this->returnOnlyOneResult($result, 'id_user');
    }

    public function getUserName($id_user){
        $pdo = $this->connect();

        $sql = "SELECT name FROM " . NAME_PREFIX . "users WHERE id_user = :id";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
        $stm->execute();

        $result = $stm->fetchAll();

        return $this->returnOnlyOneResult($result, 'name');
    }

    public function addNewUser($email, $name, $password){
        $pdo = $this->connect();

        try{
            $pdo->beginTransaction();

            //Check existing user
            $sql = "SELECT email FROM " . $this->tabPrefix("users") . " WHERE email = :email";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":email", $email, PDO::PARAM_STR);
            $stm->execute();

            $res = $stm->fetchAll();

            if(count($res) != 0)
                return false;

            //Insert into users table
            $sql = "INSERT INTO " . $this->tabPrefix("users") . " (email, name, password, active) VALUES (:email, :name, :pass, 1)";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":email", $email, PDO::PARAM_STR);
            $stm->bindParam(":name", $name, PDO::PARAM_STR);
            $stm->bindValue(":pass", md5($password), PDO::PARAM_STR);
            $stm->execute();

            $user_id = $pdo->lastInsertId();

            //Create trash money account and default account
            $sql = "INSERT INTO " . $this->tabPrefix("money_account") . " (name, id_user, visible) VALUES (:name, :id, :vis)";
            $stm = $pdo->prepare($sql);
            $stm->bindValue(":name", "Usuniete konto", PDO::PARAM_STR);
            $stm->bindParam(":id", $user_id, PDO::PARAM_INT);
            $stm->bindValue(":vis", 0, PDO::PARAM_INT);
            $stm->execute();

            $moneyTrashAccount = $pdo->lastInsertId();

            $stm->bindValue(":name", "Gotówka", PDO::PARAM_STR);
            $stm->bindParam(":id", $user_id, PDO::PARAM_INT);
            $stm->bindValue(":vis", 1, PDO::PARAM_INT);
            $stm->execute();

            //Insert into user details table info about trash account
            $sql = "INSERT INTO " . $this->tabPrefix("user_details") . " (id_user, money_account_trash) VALUES (:id_usr, :id_acc)";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id_usr", $user_id, PDO::PARAM_INT);
            $stm->bindParam(":id_acc", $moneyTrashAccount, PDO::PARAM_INT);
            $stm->execute();

            //Copying categories and subcategories from default user
            $sql = "SELECT * FROM " . $this->tabPrefix("category") . " WHERE id_user = :id_user";
            $stm = $pdo->prepare($sql);
            $stm->bindValue(":id_user", 1, PDO::PARAM_INT);
            $stm->execute();

            $res = $stm->fetchAll();

            foreach($res as $catRow){
                $sql = "INSERT INTO " . $this->tabPrefix("category") . " (name, id_user, id_color, is_income) VALUES (:name, :id_usr, :id_color, :is_income)";
                $stm = $pdo->prepare($sql);
                $stm->bindParam(":name", $catRow['name'], PDO::PARAM_STR);
                $stm->bindParam(":id_usr", $user_id, PDO::PARAM_INT);
                $stm->bindParam(":id_color", $catRow['id_color'], PDO::PARAM_INT);
                $stm->bindParam(":is_income", $catRow['is_income'], PDO::PARAM_BOOL);
                $stm->execute();

                $catId = $pdo->lastInsertId();

                $sql = "SELECT * FROM " . $this->tabPrefix("subcategory") . " WHERE id_category = :id_cat";
                $stm = $pdo->prepare($sql);
                $stm->bindParam(":id_cat", $catRow['id_category'], PDO::PARAM_INT);
                $stm->execute();

                $res1 = $stm->fetchAll();

                $sql = "INSERT INTO " . $this->tabPrefix("subcategory") . " (name, id_category) VALUES (:name, :id_cat)";
                $stm = $pdo->prepare($sql);

                foreach($res1 as $subRow){
                    $stm->bindParam(":name", $subRow['name'], PDO::PARAM_STR);
                    $stm->bindParam(":id_cat", $catId, PDO::PARAM_INT);
                    $stm->execute();
                }

                //Set default subcatgory for category
                $sql = "SELECT name FROM " . $this->tabPrefix("subcategory") . " WHERE id_sub_cat = :id_sub";
                $sql = "SELECT id_sub_cat FROM " . $this->tabPrefix("subcategory") . " WHERE name IN ($sql) AND " .
                    "id_category = :id_category";

                $stm = $pdo->prepare($sql);
                $stm->bindParam(":id_sub", $catRow['id_def_subcategory'], PDO::PARAM_INT);
                $stm->bindParam(":id_category", $catId, PDO::PARAM_INT);
                $stm->execute();

                $res1 = $stm->fetch();

                $sql = "UPDATE " . $this->tabPrefix("category") . " SET id_def_subcategory = :id_d_sub WHERE id_category = :id_cat";
                $stm = $pdo->prepare($sql);
                $stm->bindParam(":id_d_sub", $res1['id_sub_cat'], PDO::PARAM_INT);
                $stm->bindParam(":id_cat", $catId, PDO::PARAM_INT);
                $stm->execute();
            }

            $pdo->commit();

            return $user_id;
        }
        catch(PDOException $e){
            $pdo->rollBack();
            $eadmin = ADMIN_ERROR_INFO ? $e->getMessage() : '';
            $app = new AppController();
            $app->throwErrorPage(3, $eadmin);
            exit();
        }
    }

}