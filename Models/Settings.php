<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 30.12.2018
 * Time: 16:17
 */

namespace Model;

use controllers\AppController;
use PDO;
use PDOException;


class Settings extends Database
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllCategoryList($id_user)
    {
        $pdo = $this->connect();

        $sql = "SELECT cat.id_category, cat.name, col.color, cat.is_income FROM " . $this->tabPrefix("category") .
            " cat LEFT JOIN " . $this->tabPrefix("color") . " col  ON cat.id_color = col.id_color " .
            "WHERE cat.id_user = :id";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
        $stm->execute();

        return $stm->fetchAll();
    }

    public function insertNewCategory($name, $user)
    {
        $pdo = $this->connect();
        $pdo->beginTransaction();

        try {

            $sql = "INSERT INTO " . $this->tabPrefix("category") . "(name, id_user, id_color, is_income)" .
                " VALUES (:name, :id_us, 1, 0)";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id_us", $user, PDO::PARAM_INT);
            $stm->bindParam(":name", $name, PDO::PARAM_STR);
            $stm->execute();

            $lastidCat = $pdo->lastInsertId();

            $sql = "INSERT INTO " . $this->tabPrefix("subcategory") . "(name, id_category)" .
                " VALUES (:name, :id_cat)";

            $stm = $pdo->prepare($sql);
            $name = "Ogólnie - " . $name;
            $stm->bindParam(":id_cat", $lastidCat, PDO::PARAM_INT);
            $stm->bindParam(":name", $name, PDO::PARAM_STR);
            $stm->execute();

            $lastidSubCat = $pdo->lastInsertId();

            $sql = "UPDATE " . $this->tabPrefix("category") . " SET id_def_subcategory=:idSubCat WHERE id_category=:idCat";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":idSubCat", $lastidSubCat, PDO::PARAM_INT);
            $stm->bindParam(":idCat", $lastidCat, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }

    }

    public function getColorsList()
    {
        $pdo = $this->connect();

        $sql = "SELECT color FROM " . $this->tabPrefix("color");
        $stm = $pdo->prepare($sql);
        $stm->execute();

        return $stm->fetchAll();
    }

    public function deleteCategory($id_category, $id_subcategory)
    {
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();

            $sql = "UPDATE " . $this->tabPrefix("expense") . " SET id_subcategory=:idnew " .
                "WHERE id_subcategory IN (SELECT id_sub_cat FROM " . $this->tabPrefix("subcategory") .
                " WHERE id_category = :idold)";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":idnew", $id_subcategory, PDO::PARAM_INT);
            $stm->bindParam(":idold", $id_category, PDO::PARAM_INT);
            $stm->execute();

            $sql = "DELETE FROM " . $this->tabPrefix("subcategory") . " WHERE id_category=:id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_category, PDO::PARAM_INT);
            $stm->execute();

            $sql = "DELETE FROM " . $this->tabPrefix("category") . " WHERE id_category=:id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_category, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;
        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function addSubcategory($name, $parentCategory)
    {
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();
            $sql = "INSERT INTO " . $this->tabPrefix("subcategory") . " (name, id_category) VALUES (:name, :id)";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":name", $name, PDO::PARAM_STR);
            $stm->bindParam(":id", $parentCategory, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function editSubcategoryName($id_subcat, $name)
    {
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();
            $sql = "UPDATE " . $this->tabPrefix("subcategory") . " SET name=:name WHERE id_sub_cat=:id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":name", $name, PDO::PARAM_STR);
            $stm->bindParam(":id", $id_subcat, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function editCategoryName($id_subcat, $name)
    {
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();

            $sql = "UPDATE " . $this->tabPrefix("category") . " SET name=:name WHERE id_def_subcategory=:id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":name", $name, PDO::PARAM_STR);
            $stm->bindParam(":id", $id_subcat, PDO::PARAM_INT);
            $stm->execute();

            $sql = "UPDATE " . $this->tabPrefix("subcategory") . " SET name=:name WHERE id_sub_cat=:id";

            $stm = $pdo->prepare($sql);
            $name = "Ogólnie - " . $name;
            $stm->bindParam(":name", $name, PDO::PARAM_STR);
            $stm->bindParam(":id", $id_subcat, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function deleteSubcategory($id_subcategory)
    {
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();

            $sql = "SELECT id_category FROM " . $this->tabPrefix("subcategory") . " WHERE id_sub_cat=:id";
            $sql = "SELECT id_def_subcategory FROM " . $this->tabPrefix("category") . " WHERE id_category IN (" . $sql . ")";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_subcategory, PDO::PARAM_INT);
            $stm->execute();

            $id_def = $stm->fetch();
            $id_def = $id_def['id_def_subcategory'];


            $sql = "UPDATE " . $this->tabPrefix("expense") . " SET id_subcategory=:idnew WHERE id_subcategory=:id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_subcategory, PDO::PARAM_INT);
            $stm->bindParam(":idnew", $id_def, PDO::PARAM_INT);
            $stm->execute();

            $sql = "DELETE FROM " . $this->tabPrefix("subcategory") . " WHERE id_sub_cat=:id";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_subcategory, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function changeCategoryType($id_cat, bool $is_income)
    {
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();

            $sql = "UPDATE " . $this->tabPrefix("category") . " SET is_income=:is WHERE id_category=:id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":is", $is_income, PDO::PARAM_BOOL);
            $stm->bindParam(":id", $id_cat, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function categoryUpdateColor($id_cat, $color)
    {
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();

            $sql = "SELECT id_color FROM " . $this->tabPrefix("color") . " WHERE color = :col";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":col", $color, PDO::PARAM_STR);
            $stm->execute();

            $id_col = $stm->fetch();
            $id_col = $id_col['id_color'];

            $sql = "UPDATE " . $this->tabPrefix("category") . " SET id_color=:col WHERE id_category=:id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":col", $id_col, PDO::PARAM_INT);
            $stm->bindParam(":id", $id_cat, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function getCategoryListWOutDeleted($user_id, $cat_id)
    {
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();

            $sql = "SELECT is_income FROM " . $this->tabPrefix("category") . " WHERE id_category = :id_cat";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id_cat", $cat_id, PDO::PARAM_INT);
            $stm->execute();

            $id_col = $stm->fetch();
            $id_col = $id_col['is_income'];

            $sql = "SELECT cat.id_category, cat.name, col.color FROM " . $this->tabPrefix("category") .
                " cat LEFT JOIN " . $this->tabPrefix("color") . " col  ON cat.id_color = col.id_color " .
                "WHERE cat.id_user = :id AND cat.is_income = :is AND cat.id_category <> :cat";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $user_id, PDO::PARAM_INT);
            $stm->bindParam(":is", $id_col, PDO::PARAM_BOOL);
            $stm->bindParam(":cat", $cat_id, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return $stm->fetchAll();

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function changePassword($id_user, $old_pass, $new_pass)
    {
        $pdo = $this->connect();

        try {
            $sql = "SELECT password = :pass AS thesame FROM ". $this->tabPrefix("users") . " WHERE id_user = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":pass", md5($old_pass), PDO::PARAM_STR);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $res = $stm->fetch();

            if($res['thesame'] == 0){
                return -1;
            }

            $pdo->beginTransaction();
            $sql = "UPDATE " . $this->tabPrefix("users") . " SET password = :pass WHERE id_user = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":pass", md5($new_pass), PDO::PARAM_STR);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();
            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function deleteMoneyAccount($id_account, $id_user){
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();
            $sql = "SELECT money_account_trash FROM ". $this->tabPrefix("user_details") . " WHERE id_user = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $res = $stm->fetch();

            $res = $res['money_account_trash'];

            $sql = "UPDATE " . $this->tabPrefix("expense") . " SET id_m_acc = :idacc WHERE id_m_acc = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":idacc", $res, PDO::PARAM_INT);
            $stm->bindParam(":id", $id_account, PDO::PARAM_INT);
            $stm->execute();

            $sql = "DELETE FROM " .$this->tabPrefix("money_account") . " WHERE id_m_acc = :id";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_account, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }

    }

    public function createMoneyAccount($name, $id_user){
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();
            $sql = "INSERT INTO ". $this->tabPrefix("money_account") . " (name, id_user, visible) VALUES (:name, :id, 1)";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":name", $name, PDO::PARAM_STR);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }

    }

    public function updateMoneyAccount($id_account, $name){
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();
            $sql = "UPDATE ". $this->tabPrefix("money_account") . " SET name = :name WHERE id_m_acc = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":name", $name, PDO::PARAM_STR);
            $stm->bindParam(":id", $id_account, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function deleteUser($password, $id_user){
        $pdo = $this->connect();

        try {
            $sql = "SELECT password = :pass AS thesame FROM ". $this->tabPrefix("users") . " WHERE id_user = :id";

            $password = md5($password);

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":pass", $password, PDO::PARAM_STR);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();
            $res = $stm->fetch();

            if($res['thesame'] == 0){
                return false;
            }


            $pdo->beginTransaction();
            $sql = "DELETE FROM " . $this->tabPrefix("user_details") . " WHERE id_user = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $sql = "SELECT id_m_acc FROM " . $this->tabPrefix("money_account") . " WHERE id_user = :id";
            $sql = "DELETE FROM " . $this->tabPrefix("expense") . " WHERE id_m_acc IN($sql)";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $sql = "SELECT id_category FROM " . $this->tabPrefix("category") . " WHERE id_user = :id";
            $sql = "DELETE FROM " . $this->tabPrefix("subcategory") . " WHERE id_category IN($sql)";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $sql = "DELETE FROM " . $this->tabPrefix("category") . " WHERE id_user = :id";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $sql = "DELETE FROM " . $this->tabPrefix("money_account") . " WHERE id_user = :id";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $sql = "DELETE FROM " . $this->tabPrefix("users") . " WHERE id_user = :id";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

}